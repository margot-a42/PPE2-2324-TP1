rendu pour séance du 31/01 : 

Je n'ai presque pas eu de problème avec les exercices sur gitlearnbranching, j'ai trouvé que c'était une façon ludique et plus claire de travailler les commandes git. Pour le troisième exercice je n'ai pas réussi à "déplacer" la branche main sur le commit C11.
Quant à la feuille de td, je ne pense pas avoir eu de problèmes sur les premiers exercices (j'ai réussi localement mais je ne suis pas sûre que le commit des branches ait fonctionné), mais lors de l'exercice 3 question 2/3, je n'ai pas eu de conflit lors du merge. Je préfère donc attendre la correction en cours pour mieux comprendre.
